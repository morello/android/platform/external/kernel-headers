/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */
#ifndef _UAPI__ASM_CHERI_H
#define _UAPI__ASM_CHERI_H

#define CHERI_PERM_SW_VMEM	(1 << 2) /* User[0] permission */

#endif /* _UAPI__ASM_CHERI_H */
